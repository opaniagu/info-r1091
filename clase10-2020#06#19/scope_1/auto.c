#include <stdio.h>

int main(){
    // en este bloque no existe k
    auto int i=1, j=2; 
    printf(" i = %d | &i = %p\n", i, &i);
    printf(" j = %d | &j = %p\n", j, &j);
    {
        // la variable int j ya no es visible 
        float j=3;
        int k = 10;
        j=i+j;
        printf(" i = %d | &i = %p\n", i, &i);
        printf(" j = %.1f | &j = %p\n", j, &j);
    }
    // error: 'k' undeclared (first use in this function)
    // printf(" k = %d | &k = %p\n", k, &k);
    return 0;
}