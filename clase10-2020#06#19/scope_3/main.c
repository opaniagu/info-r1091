/*
 * gcc -c main.c -o main.o
 * gcc -c file.c -o file.o
 * gcc -o scope main.o file.o
 */

#include <stdio.h>
#include "file.h"

int main(){

    printf(" j = %d | &j = %p\n", j, &j);
    f();
    printf(" j = %d | &j = %p\n", j, &j);

    return 0;
}
