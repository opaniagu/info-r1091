/*
 * crear variables y asignarles valor para luego imprimirlas.
 *
 */

#include <stdio.h>

int main (void) {
        
    // declaracion
    int var1;
    float var2;
    char var3;
   
    // inicializacion
    var1 = 15;
    
    // des-comentar para probar y comentar la asignacion anterior
    // var1 = 15.5;
    
    var2 = 25.92;
    
    var3 = 30;
    
    // des-comentar para probar y comentar la asignacion anterior
    // var3 = 129;
    
    printf("El valor de var1 es: %d, el valor de var2 es: %.2f y el valor de var3 es %d\n",var1,var2,var3);
    
    return 0;  
    
    
    
}








