#include <stdio.h>
#include <string.h>

/*
 *  1) inicialización de arrays de string
 *  2) tamaño/longitud de un array de string
 *  3) recorrer un array de string
 *  4) llamar a una funcion pasando un elemento del array de string ( array unidimensional o string) 
 *  5) llamar a una funcion pasando el array de string
 *  6) array de punteros 
 *  7) array asociados 
 */

#define MAX_LEN_ARR 2
#define MAX_LEN_STR 50

void str_upper(char *);
void printf_array_string(char array[][MAX_LEN_STR], int size);

int main(){

    // variables auxiliares
    int arr_size_elements, arr_size_bytes, i;
    
    /*
     * inicialización de arrays de string
     */

    // declaración de arrays de string
    char arr_str[MAX_LEN_ARR][MAX_LEN_STR];
    
    // inicialización de arrays de string
    strcpy(arr_str[0], "New");
    strcpy(arr_str[1], "Used");

    
    // declaración e inicialización de arrays de string
    char arr_str_state[MAX_LEN_ARR][MAX_LEN_STR] = {"New", "Used"};   
    
    // Nota:
    // La siguiente forma de declarar e inicializar no esta permitida
    // char arr_str_state_bad[][] = {"New", "Used"};
    
    // declaración e inicialización de arrays de string
    char arr_str_state_good[][MAX_LEN_STR] = {"New", "Used"}; 

    /*
     * tamaño de un array de strings
     */
    
    arr_size_bytes = sizeof(arr_str);
    arr_size_elements = sizeof(arr_str) / sizeof(arr_str[0]);
    printf("La Longitud del array 'arr_str' en bytes es %d y tiene %d elementos\n", arr_size_bytes, arr_size_elements);

    arr_size_bytes = sizeof(arr_str_state);
    arr_size_elements = sizeof(arr_str_state) / sizeof(arr_str_state[0]);
    printf("La Longitud del array 'arr_str_state' en bytes es %d y tiene %d elementos\n", arr_size_bytes, arr_size_elements);

    arr_size_bytes = sizeof(arr_str_state_good);
    arr_size_elements = sizeof(arr_str_state_good) / sizeof(arr_str_state_good[0]);
    printf("La Longitud del array 'arr_str_state_good' en bytes es %d y tiene %d elementos\n", arr_size_bytes, arr_size_elements);

    /*
     * recorrer un array de string
     */
    for(i=0; i<arr_size_elements; i++){
        printf("En el indice %d el array tiene el valor %s\n", i, arr_str_state_good[i]);
    }

   
    /*
    * llamar a una funcion pasando un elemento del array 
    * de string ( array unidimensional o string) 
    */
    i = 1;
    str_upper(arr_str_state_good[1]);
    printf("En el indice %d el array tiene el valor %s\n", i, arr_str_state_good[i]);
    
    /*
     * llamar a una funcion pasando el array de string
     */
    printf("\n");
    printf_array_string(arr_str_state_good, arr_size_elements);


    return 0;
}

// a = 97
// A = 65
void str_upper(char *s){

    while( *s != 0) {
        if ( *s >= 'a' && *s <= 'z' ){    
            *s = *s - ('a' - 'A');
             
        }
        s++;
    }
}

void printf_array_string(char array[][MAX_LEN_STR], int size){
    int i;

    for( i=0; i<size; i++){
         printf("En el indice %d el array tiene el valor %s\n", i, array[i]);
    }
}
