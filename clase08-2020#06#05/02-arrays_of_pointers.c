#include <stdio.h>
#include <string.h>

#define SIZE 7

int main(void){

    int i, size, size_bytes;
    char aux;
    char my_array[] = "array";
    char* my_ptr = "pointer";

    printf("my_array is:\t %s\n", my_array);
    printf("my_ptr is:\t %s\n", my_ptr);
    
    // podriamos hacer esto, aunque permderiamos la referencia a la cadena "pointer"
    my_ptr = my_array;
    printf("my_array is:\t %s\n", my_array);
    printf("my_ptr is:\t %s\n", my_ptr);
    
    // entonces un array de punteros, no es otra cosa que un array que cada
    // elemento es un puntero (dirección)
    // en el ejemplo a continuacion, cada elemento es un puntero a un caracter
    // char* my_ptr_arr[2];

    // como podemos inicializarlo:
    char* my_ptr_arr[2] = {"New", "Used"};

    size_bytes = sizeof(my_ptr_arr); 
    size = sizeof(my_ptr_arr)/ sizeof(my_ptr_arr[0]);
    printf("El tamanio del array en bytes %d y de elementos es %d\n", size_bytes, size);

    for (i=0; i<size;i++){
        printf("%s\n", my_ptr_arr[i]);
    }
    
    // recorrerlo con un puntero a char
    char *p;
    p = my_ptr_arr[0];
    printf("----\n");
    for (i=0; i<10;i++){
        printf("%c", *(p+i));
    };
    printf("\n----\n");
    
    // cual es el tamanio en bytes del array y sus datos apuntados de my_ptr_arr ?
    
    return 0;

}
