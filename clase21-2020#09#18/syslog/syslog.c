#include <syslog.h>

/*
 * Ejemplos:
 * 
 * Sep 16 11:35:16 e2349bfc3b70 info-utn[1676]: Start logging
 * Sep 16 18:20:28 e2349bfc3b70 a.out: Start logging
 * Sep 16 18:20:44 e2349bfc3b70 a.out: Start logging
 */

int main()
{

    syslog(LOG_INFO, "Start logging");

    openlog("info-utn", LOG_PID, LOG_USER | LOG_DEBUG);
    syslog(LOG_INFO, "Start logging");
    closelog();

    return 0;
}
