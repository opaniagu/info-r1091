#include<math.h>
#include<string.h>
#include<stdio.h>

void string_reverse (char* str);

int obtener_numero (char * str)
{
    
    int len = strlen (str);
    int numero=0;
    int i;
    for (i=0;i<len;i++)
    {
        switch (str[i])
            {
                case '$':
                  numero+=0*pow (8,len-i-1);
                    break;
                case '%':
                  numero+=1*pow(8,len-i-1);
                    break;
                case '&':
                    numero+=2*pow(8,len-i-1);
                  break;
                case '*':
                  numero+=3*pow(8,len-i-1);
                    break;
                case '@':
                  numero+=4*pow(8,len-i-1);
                    break;
                case '!':
                  numero+=5*pow(8,len-i-1);
                    break;
                case '+':
                  numero+=6*pow(8,len-i-1);
                    break;    
                case '=':
                  numero+=7*pow(8,len-i-1);
                    break;
                default:
                    break;
            }
    }
    
    return numero;
}

void itoa (char* dest,int numero)
{
    int i;
    for(i=0;numero;i++)
    {
        dest[i]=numero%10+'0';
        numero = numero/10;
    
    }
    dest [i]='\0';
    string_reverse (dest);
}

void desencriptar_numero (char* src, char* dest)

{
    int numero,i,j,len;
    char encriptado [100];
    char ascii [100];
    len = strlen(src);
    for (i=0;i<len && src[i] != '#' ;i++); // encuentro donde empieza el numero
    for (j=i+1;src[j] != '#';j++); // encuentro donde termina el numero
    strncpy(encriptado,&src[i+1],j-i-1);
    encriptado [j-i-1]='\0';
    numero = obtener_numero(encriptado);

    itoa (ascii,numero);
    strncpy(dest,src,i);
    strcat(dest,ascii);
    strcat(dest,&src[j+1]);
    
    
}
