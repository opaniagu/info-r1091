/*
 0 - $
 1 - %
 2 - &
 3 - *
 4 - @
 5 - !
 6 - +
 7 - = 
 */
#include<string.h>
#include<stdlib.h>
void string_reverse (char* str);
int extraer_numero (char* str);
void crypto (int numero,char* dest);

void encriptar_numero (char* src, char* dest)

{

    int numero,i,j,len;
    char octal [10];
    numero = extraer_numero(src);
    len = strlen(src);
    crypto(numero,octal);

    for (i=0;i<len && (src[i] <= '0' || src[i]>='9');i++); // encuentro donde empieza el numero
    for (j=i;src[j] >= '0' && src[j]<='9';j++); // encuentro donde termina el numero

    strncpy(dest,src,i);
    strcat(dest,octal);
    strcat(dest,&src[j]);
    
    
}


int extraer_numero (char* str)
{
    int len = strlen (str);
    int i,j;
    char aux[10];
    for (i=0;i<len && (str[i] <= '0' || str[i]>='9');i++); // encuentro donde empieza el numero
    for (j=i;str[j] >= '0' && str[j]<='9';j++); // encuentro donde termina el numero
    strncpy (aux,&str[i],j); // copio los numeros a la variable auxiliar
    return atoi (aux); // convierto el texto en numeros
    
}

 void crypto (int numero,char* dest)
{
    int i;
    dest[0]='#';
    for(i=1;numero;i++)
    {
        
        switch (numero%8)
        {
            case 0:
                dest[i]='$';
                break;
            case 1:
                dest[i]='%';
                break;
            case 2:
                dest[i]='&';
                break;
            case 3:
                dest[i]='*';
                break;
            case 4:
                dest[i]='@';
                break;
            case 5:
                dest[i]='!';
                break;
            case 6:
                dest[i]='+';
                break;    
            case 7:
                dest[i]='=';
                break;
            default:
                dest[i]='#';
                break;
                
        }    
        numero = numero/8;
    
    }
    dest [i]='#';
    dest [i+1]='\0';
    string_reverse (dest);
}

void string_reverse (char* str)
{
    
    int len = strlen(str);
    int i;
    char aux;
    for (i=0;i<len/2;i++)
    {
        aux=str[i];
        str[i]=str[len-1-i];
        str[len-1-i]=aux;
    }
}


