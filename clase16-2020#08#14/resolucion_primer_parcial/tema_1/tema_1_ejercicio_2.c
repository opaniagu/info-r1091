#include <stdio.h>  // printf
#include <string.h> // memset, strlen, strcpy

#define STR_MAX 100

#define OK       1
#define ERROR   -1


int remove_space(char *str);
int syntax_check(char *str);

int main(void){

    //ok
    char sensor_data_1[] = "{ 1, 5.2, true }";
    char sensor_data_2[] = "{ 2, 1.1, false}";
    
    //nok
    char sensor_data_3[] = "2, 1.1, false}";
    char sensor_data_4[] = "{ 1.1, false";
    
    int r;

    r = syntax_check(sensor_data_1);
    printf("La sintaxis es %d\n", r);
 
    r = syntax_check(sensor_data_2);
    printf("La sintaxis es %d\n", r);
 
    r = syntax_check(sensor_data_3);
    printf("La sintaxis es %d\n", r);
 
    r = syntax_check(sensor_data_4);
    printf("La sintaxis es %d\n", r);
 

    return 0;
}


int remove_space(char *data){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_space = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != 32 ){
            aux[j] = data[i];
            
            j++;
        }
        else {
            count_space++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de espacios eliminados
    return count_space;
}


int syntax_check(char *data){
    int l;
    int count_comma=0;
  
    // no lo pedia
    l = strlen(data);
    if ( l == 0 )
        return ERROR;

    // remove todos los espacios
    remove_space(data);

    // El string debe empezar con el carácter '{'
    //primer caracter '{'
    if (data[0] != '{')
        return ERROR;

    //ultimo caracter '}'   
    if (data[l-1] != '}')
        return ERROR;

    // encontrar dos comas ( es decir tres parametros)
    while( *data != 0){
        if ( *data == ',')
            count_comma++;
        data++;
    }
    if ( count_comma != 2)
        return ERROR;

    return OK;

}