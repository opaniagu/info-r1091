# Clase #7

## Cadenas de Caracteres

Se presentaron las cadenas de carateres.

Una cadena es un arreglo de caracteres, o sea, del tipo char.

Se realizaron ejercicios con funciones de cadenas.

- strlen
- strcpy
- strcat
- strcmp

Se utilizaron las mismas funciones con las ofrecidas por libc en "string.h"

Para comprender su utilizaron:

- man 3 strlen
- man 3 strcpy
- man 3 strcat
- man 3 strcmp


