#include "database.h"

FILE* db_open ( char* file_name ){
    FILE *fp;

    fp = fopen(file_name, "a+");
    return fp;
}

int db_close( FILE * file_name){
    return fclose(file_name);
}

int write_record( FILE* fp, PERSONA* p){
    //TODO: verificaciones ( archivo, PERSONA)

    // me posiciono al final del archivo
    if ( fseek(fp, 0, SEEK_END) !=0) {
    //TODO:
        return DATABASE_WRITE_NOK;
    }

    if( fwrite(p, sizeof(PERSONA), 1, fp) == 0){
    //TODO:
        return DATABASE_WRITE_NOK;
    }
    return DATABASE_WRITE_OK;
}


// funcion que lee un registro
// busca por el campo id
// suponemos que el id es unico y que los registros no estan ordenados
int read_record(FILE* fp, int id, PERSONA* p){
   PERSONA aux;
   int r;

    //TODO: realizar las verificaciones que consideren 

    // me posiciono al principio
    if ( fseek(fp, 0, SEEK_SET) !=0) {
    //TODO:
        return DATABASE_READ_NOK;
    }

    while ( (r = fread(&aux, sizeof(PERSONA), 1, fp)) != 0){
        if ( aux.id == id ){
            //copio la estructura a 'p'
            *p = aux;
            return DATABASE_READ_FOUND;
        }
    }
    return DATABASE_READ_NOT_FOUND;

}

void print_record( PERSONA* p){
    if ( p != NULL){
        printf("----------------\n");
        printf("id: %d\n", p->id);
        printf("nombre: %s\n", p->nombre);
        printf("edad: %d\n", p->edad);
        printf("altura: %f\n", p->altura);
        printf("peso: %f\n", p->peso);
        printf("----------------\n");
    }
    else{
        printf("database.c::print_record():: Registro recibido NULL\n");
    }
}

void cargar( FILE* fp, PERSONA** database){
    PERSONA aux;
    int count=0;
    //TODO: verificar

    if ( fseek(fp, 0, SEEK_SET) != 0){
        //TODO
        exit(-1);
    }

    while ( (fread( &aux, sizeof(PERSONA), 1, fp)) != 0){
        *database = (PERSONA*)malloc(sizeof(PERSONA));
        //TODO: comprobar malloc

        //copio estructura
        **database = aux;
        database++;
        count++;
    }

    printf("database.c::cargar(): Se cargaron %d registros\n", count);

}

void listar(PERSONA** database){
    //TODO: verificar

    while ( *database != 0){
        print_record ( *database);
        database++;
    }

}