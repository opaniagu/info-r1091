#include<stdio.h>
#include <stdlib.h>

int main (int argc, char ** argv)
{
    
    
    if (argc < 3)
    {
        printf("argumentos insuficientes");
        exit (0);
        
    }
    
    FILE * fp_read;
    FILE * fp_write;
    char byte;
    char count=0;
    fp_read = fopen (argv [1],"r");
    fp_write = fopen (argv [2],"w");
    


    while (fread (&byte,1,1,fp_read))
    {
        fprintf(fp_write,"  %02x",byte);
        count ++;
        if (count == 15)
        {
            fprintf(fp_write,"\n");
            count = 0;
            
        }
    }
    
    
    fclose (fp_read);
    fclose (fp_write);
   return 0; 
}

