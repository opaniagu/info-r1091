# Informatica I

_Repositorio de la materia Informatica I de la Carrera de Electronica de la Universidad Tecnologica Nacional_


## Guia de Trabajos Practicos

|  Fecha  	|  Descripcion  |
| :-- 		| --: 			|
|   08-05  	| [Control de Flujo](#Control-de-Flujo)   |
|   22-05 	| [Arrays parte 1](#Arrays-parte-1)    |


### Control de Flujo

- scanf %c

Mas adelante veremos que existen mejores alternativas para lectura del teclado
que scanf. Pero para ello primero debemos ver strings o cadenas de caracteres.

Cuando se lee del teclado un caracter con scanf, por ejemplo ingresamos 'S' y enter '\n', el caracter
'\n' permanece en un buffer intermedio y si realizamos una proxima lectura de teclado producira un 
funcionamiento inesperado.

Entonces cuando utilicemos scanf con el especificador de formato %c, re-escribir el codigo de la siguiente manera:

```c
scanf(" %c", &ch);
       ^
       |
   espacio
``` 


Colocar este espacio antes de %c, saltea todos los caracteres denominados 'whitespace' (incluido el \n) y leer el primer caracter non-whitespace.

 




```c
/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final la suma de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 * Preguntas:
 *      - ¿Cual de las "estructuras de repeticion" le resulto mas conveniente?
 */

#include <stdio.h>

int main(){
  
      return 0;
}
```

```c
/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final la "suma" y el "promedio" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>


int main(){

    // desarrollar...



    return 0;
}
```

```c
/*
 * Realizar un programa que permita la carga de valores enteros por teclado. 
 * Luego de ingresar el valor mostrar un mensaje por pantalla que pida confirmar 
 * al usuario si desea cargar otro valor ingresando los caracteres 'S' o 'N'. 
 * Mostrar al final el valor "minimo" y el valor "maximo" de los valores ingresados.
 * Solo se pueden utilizar las siguientes funciones: 
 *      printf
 *      scanf
 *      for/while/do while
 *
 */

#include <stdio.h>

int main(){

    // desarrollar...



    return 0;
}
```



### Arrays parte 1

```c
/* Devuelve el indice del primer
 * element encontrado. 1 si no lo
 * encuentra
 */
int array_find (int *, int)
```

```c
/* Devuelve 1 si son iguales.
 * 1 si
 * son distintos
 */
int array_equal (int*, int, int*, int)
```

```c
/* Inicializa el array con el valor dado
 */
void array_fill (int *, int, int)
```

```c
/* Copia un array en
 * otro. 1 en caso
 * de que los tamaños no sean iguales
 */
int array_copy ( int *, int , int*, int)
```

```c
/* Devuelve el promedio */
float array_average (int *, int)
```


