#include "pipe.h"

int main()
{
	int	fd;						//File descriptor
	char buffer[MAX_CHARS];		//Buffer para guardar el mensaje
	int	cant;					//Variable para guardar la cantidad de caracteres escritos en el FIFO
	
	// Abrimos el FIFO, y si no existe lo creamos: En cualquier caso fd se queda con el File descriptor
	if ((fd = open(FIFO_NAME, O_WRONLY)) == -1)
	  if ((fd = mkfifo (FIFO_NAME, 0666)) < 0)
	    fprintf (stderr, "Error creando la FIFO %s. Código de error %s\n",FIFO_NAME,strerror(errno));

	// Vamos a esperar una línea desde el teclado
	printf ("Por favor ingrese un mensaje: \n");

	if (!fgets(buffer, MAX_CHARS, stdin))
	    fprintf (stderr, "Error leyendo standard input. Código de error %s\n", strerror(errno));
	
	//Vamos a escribir en el FIFO
	cant = write (fd, buffer, strlen (buffer));
	
	printf("Se escribieron %d caracteres\n", cant);
	
	close (fd);
	
	return 0;
}