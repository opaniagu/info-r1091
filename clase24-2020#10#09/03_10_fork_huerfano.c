#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
	printf("Ejemplo de proceso huérfano.\n");
	printf("Inicio del proceso padre. PID=%d\n", getpid());

	if (fork() == 0)
	{
		printf("Inicio proceso hijo. PID=%d, PPID=%d\n", getpid(), getppid());
		sleep(1);
		printf("El proceso queda huérfano. PID=%d PPID=%d\n", getpid(), getppid());
	}
	else
		printf("Soy el padre. PID=%d\n", getpid());

	printf("Fin del proceso %d\n", getpid());

	exit(0);
}
